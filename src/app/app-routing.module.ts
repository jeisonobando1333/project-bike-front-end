import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {
    path: 'index',
    loadChildren: () => import('../app/modules/main-home/main-home.module')
    .then(m => m.MainHomeModule)
  },
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  },
  {
    path:'catalogo',
    loadChildren: () => import('../app/modules/catalogo/catalogo.module')
    .then(m => m.CatalogoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
