import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainHomeRoutingModule } from './main-home-routing.module';
import { SlideComponent } from './slide/slide.component';
import { IndexComponent } from './index/index.component';


@NgModule({
  declarations: [SlideComponent, IndexComponent],
  imports: [
    CommonModule,
    MainHomeRoutingModule
  ]
})
export class MainHomeModule { }
