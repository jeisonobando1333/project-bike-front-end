import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogoRoutingModule } from './catalogo-routing.module';
import { MainCatalogoComponent } from './main-catalogo/main-catalogo.component';
import { ModalSuscriptionComponent } from './modal-suscription/modal-suscription.component';


@NgModule({
  declarations: [
    MainCatalogoComponent,
    ModalSuscriptionComponent
  ],
  imports: [
    CommonModule,
    CatalogoRoutingModule,
  ]
})
export class CatalogoModule { }
