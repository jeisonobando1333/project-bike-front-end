import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-suscription',
  templateUrl: './modal-suscription.component.html',
  styleUrls: ['./modal-suscription.component.styl']
})
export class ModalSuscriptionComponent implements OnInit {

  constructor( public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  close(): void {
    this.activeModal.close();
  }
  dismiss(): void {
    this.activeModal.close();
  }
}
