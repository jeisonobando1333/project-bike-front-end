import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalSuscriptionComponent} from '../modal-suscription/modal-suscription.component';

@Component({
  selector: 'app-main-catalogo',
  templateUrl: './main-catalogo.component.html',
  styleUrls: ['./main-catalogo.component.styl'],
  providers: [NgbModal] // nose por que se usa aqui ajja
})
export class MainCatalogoComponent implements OnInit {
  constructor(private modalService: NgbModal) {
   }
   open(): void {
     this.modalService.open(ModalSuscriptionComponent)     
   }
  ngOnInit(): void {
    this.open();
  }

}
